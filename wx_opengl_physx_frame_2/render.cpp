#include "render.h"

#include "zpv/Zpv.h"
#include "zglor/Zglor.h"
#include <GL>

void RenderCallback(int _width, int _height)
{

	static unsigned int previousTime = 0;
	unsigned int currentTime = timeGetTime();
	unsigned int elapsedTime = currentTime - previousTime;

	if( elapsedTime < 10.0f )
		return;

	previousTime = currentTime;

	// Clear buffers -- do it now so we can render some debug stuff in tickCar.
	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
	
	// zpv begin
	zpv::InputManager::getSingletonRef().simulate();
	// zpv end
	
	// Setup camera
	glMatrixMode( GL_PROJECTION );
	glLoadIdentity();
	gluPerspective( 60.0f, (float)_width / (float)_height,
		            1.0f,
					10000.0f );
	
	if( zpv::VehicleManager::getActiveVehicle() != NULL )
	{
		glMatrixMode( GL_MODELVIEW );
		glLoadIdentity();

		#define BUFFERSIZE 25
		static NxMat34 mbuffer[ BUFFERSIZE ];
		static int frameNum = 0;
		int index = frameNum % BUFFERSIZE;
  	
		NxMat34 camera, cameraInv;
		NxVec3 cameraZ, lookAt, cr, tmp;
		NxF32 cameraInvMat[ 16 ];

		NxMat34 car = zpv::VehicleManager::getActiveVehicle()->getGlobalPose();
	  
		car.M.getColumn( 2,  cr   );
		car.M.getColumn( 0,  tmp  );
		car.M.setColumn( 0,  cr   );
		car.M.setColumn( 2, -tmp  );

		if( frameNum == 0 )
		{
			for( int i = 0; i < BUFFERSIZE; i++ )
				mbuffer[ i ] = car;
		}

		camera = mbuffer[ index ];
		mbuffer[ index ] = car;

		camera.t.y += zpv::VehicleManager::getActiveVehicle()->getCameraDistance() * 0.5f;	//camera height

		camera.M.getColumn( 2, cameraZ );
		camera.t += ( zpv::VehicleManager::getActiveVehicle()->getCameraDistance() * cameraZ );

		lookAt = ( camera.t - car.t );
		lookAt.normalize();

		camera.M.setColumn( 2, lookAt );
		cr = NxVec3( 0, 1, 0).cross( lookAt );
		cr.normalize();
		camera.M.setColumn( 0, cr );
		cr = lookAt.cross( cr );
		cr.normalize();
		camera.M.setColumn( 1, cr );

		camera.getInverse( cameraInv );

		cameraInv.getColumnMajor44( cameraInvMat );

		glMultMatrixf( cameraInvMat );

		zpv::InputManager::getSingletonRef().updateCamera( camera.t, -lookAt );

		frameNum++;
	}
	else
	{
		zpv::InputManager::getSingletonRef().cameraControls();

		glMatrixMode( GL_MODELVIEW );
		glLoadIdentity();

		NxVec3 Eye = zpv::InputManager::getSingletonRef().getEye();
		NxVec3 Dir = zpv::InputManager::getSingletonRef().getDir();

		gluLookAt( Eye.x,
			       Eye.y,
				   Eye.z,
				   Eye.x + Dir.x,
				   Eye.y + Dir.y,
				   Eye.z + Dir.z,
				   0.0f,
				   1.0f,
				   0.0f );
	}

	//render ground plane:
	glColor4f( 0.4f, 0.4f, 0.4f, 1.0f );

	static NxVec3 pPlaneVertexBuffer[ 200 * 4 ];
	static bool init = false;
	
	if( !init )
	{
		init = true;

		for( int i = 0; i < 200; i++ )
		{
			float v = ( float )i * 2 - 200.0f;
			pPlaneVertexBuffer[ i * 4 + 0].set( v,       0.05f, -200.0f );
			pPlaneVertexBuffer[ i * 4 + 1].set( v,       0.05f,  200.0f );
			pPlaneVertexBuffer[ i * 4 + 2].set( -200.0f, 0.05f,  v      );
			pPlaneVertexBuffer[ i * 4 + 3].set( 200.0f,  0.05f,  v      );
		}
	}
	if( zpv::InputManager::getSingletonRef().isDebugVisualization() )
	{
		glEnableClientState( GL_VERTEX_ARRAY );
		glVertexPointer(3, GL_FLOAT, sizeof(NxVec3), pPlaneVertexBuffer);
		glDrawArrays(GL_LINES, 0, 200*4);
		glDisableClientState(GL_VERTEX_ARRAY);
	}

	glPushMatrix();

	RenderTerrain();

	if( zpv::InputManager::getSingletonRef().isDebugVisualization() ) 
	{
		zpv::DebugRenderer::getSingletonRef().renderData();
	} 
	else 
	{
		RenderAllActors();
	}

	glPopMatrix();
	zpv::VehicleManager::drawVehicles();

	static zpv::CreationModes _oldCreationMode = zpv::MODE_NONE;
	static zglor::OrthographicDrawing orthoDraw;

	orthoDraw.setOrthographicProjection( (float)_width, (float)_height );
	glDisable(GL_LIGHTING);
	glDisable(GL_DEPTH_TEST);
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	if(_oldCreationMode != zpv::MODE_CAR)
		_oldCreationMode = zpv::MODE_CAR;

	renderHUD( orthoDraw, _width, _height );

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();
	orthoDraw.resetPerspectiveProjection();
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_LIGHTING);

}

void RenderTerrain()
{
	static bool init = false;

	static float pVertList[TERRAIN_NB_FACES*3*3];
	static float pNormList[TERRAIN_NB_FACES*3*3];
	static float pColorList[TERRAIN_NB_FACES*3*4];

	if(!init)
	{
	    init = true;
		int vertIndex = 0;
		int normIndex = 0;
		int colorIndex = 0;
		for(int i=0;i<TERRAIN_NB_FACES;i++)
		{
			NxMaterialIndex mat = zpv::TerrainManager::getSingletonRef().getTerrainMaterials()[ i ];
			float r,g,b;
			zpv::MaterialFactory* materialFactory = zpv::MaterialManager::getSingletonRef().factoryForScene();
			if( mat == materialFactory->getIce() )
			{
				r = 1;
				g = 1;
				b = 1;
			} 
			else 
			{
				if( mat == materialFactory->getRock() )
				{
					r = 0.3f;
					g = 0.3f;
					b = 0.3f;
				} 
				else
				{
					if( mat == materialFactory->getMud() )
					{
						r = 0.6f;
						g = 0.3f;
						b = 0.2f;
					} 
					else
					{
						r = 0.2f;
						g = 0.8f;
						b = 0.2f;
					}
				}
			}

			const zpv::TerrainManager::TerrainVertsArray& terrainVerts = 
				zpv::TerrainManager::getSingletonRef().getTerrainVerts();

			const zpv::TerrainManager::TerrainFacesArray& terrainFaces =
			    zpv::TerrainManager::getSingletonRef().getTerrainFaces();

			const zpv::TerrainManager::TerrainNormalsArray& terrainNormals =
				zpv::TerrainManager::getSingletonRef().getTerrainNormals();

			for( int j = 0; j < 3; j++ )
			{
				pVertList[vertIndex++] = terrainVerts[terrainFaces[i*3+j]].x;
				pVertList[vertIndex++] = terrainVerts[terrainFaces[i*3+j]].y;
				pVertList[vertIndex++] = terrainVerts[terrainFaces[i*3+j]].z;

				pNormList[normIndex++] = terrainNormals[terrainFaces[i*3+j]].x;
				pNormList[normIndex++] = terrainNormals[terrainFaces[i*3+j]].y;
				pNormList[normIndex++] = terrainNormals[terrainFaces[i*3+j]].z;

				pColorList[colorIndex++] = r;
				pColorList[colorIndex++] = g;
				pColorList[colorIndex++] = b;
				pColorList[colorIndex++] = 1.0f;

			}
		}
	}

	else
	{
		glEnableClientState( GL_VERTEX_ARRAY );
		glVertexPointer( 3, GL_FLOAT, 0, pVertList );
		glEnableClientState( GL_NORMAL_ARRAY );
		glNormalPointer( GL_FLOAT, 0, pNormList );

		glEnableClientState(GL_COLOR_ARRAY);
		glColorPointer(4, GL_FLOAT, 0, pColorList);
		
		
		glDrawArrays(GL_TRIANGLES, 0, TERRAIN_NB_FACES*3);
		glDisableClientState(GL_COLOR_ARRAY);
		glDisableClientState(GL_NORMAL_ARRAY);
		glDisableClientState(GL_VERTEX_ARRAY);
	}
}

void ReshapeCallback(int width, int height)
{
	glViewport(0, 0, width, height);
}

void RenderAllActors()
{
	glColor4f(0.6f,0.6f,0.6f,1.0f);
	NxScene* scene = zpv::PhysXManager::getSingletonRef().getDefaultScene();
	for(unsigned int i=0;i<scene->getNbActors();i++)
	{
		zglor::ObjectRenderer::getSingletonRef().drawActor( scene->getActors()[ i ] );
	}
}

void renderHUD(zglor::OrthographicDrawing& orthoDraw, int _width, int _height)
{
	zpv::Vehicle* curVehicle = static_cast< zpv::Vehicle* >( zpv::VehicleManager::getActiveVehicle() );
	if(curVehicle == NULL)
	{
		return;
	}
	int width = _width;
	int height = _height;

	NxReal maxV = curVehicle->getMaxVelocity();
	NxReal velocity = curVehicle->getDriveVelocity();
	NxReal velocity01 = velocity / maxV;

	NxReal radius = 50;
	glColor4f(1,1,1,1);
	NxReal kmh = velocity * 3.6f;
	NxReal maxKmh = maxV * 3.6f;
	NxReal spacing = 10.f;
	float centerX = width-radius-spacing;
	float centerY = height-radius-spacing;
	float centerY2 = height-30.f-3*radius;

	char buf[100];
	sprintf_s(buf, "km/h: %3d", (int)kmh);
	orthoDraw.drawText( (int)(centerX-35), (int)(centerY+radius-10.f), buf, _width, _height );

	// RPM
	if(curVehicle->getMotor())
	{
		char buf[100]; sprintf_s(buf, "Rpm: %4d", (int)curVehicle->getMotor()->getRpm());
		orthoDraw.drawText((int)(centerX-35), (int)(centerY2+radius-10.f), buf, _width, _height );
	}

	//if(curVehicle->getGears())
	//{
	//	char buf[100]; sprintf_s(buf, "Gear: %d", curVehicle->getGears()->getGear());
	//	orthoDraw.drawText((int)(centerX-20.f), (int)(centerY2-radius-10.f), buf, _width, _height );
	//}

	glScalef(1, -1, 1);
	glTranslatef(0, -orthoDraw.mHeight, 0);

	// Velocity
	float angle = (1-velocity01) * NxMath::degToRad(210.f) + velocity01*NxMath::degToRad(-30.f);
	glColor4f(GL_COLOR_WHITE);
	orthoDraw.drawCircle(centerX , centerY, radius, -30, 210, 10, (int)(maxKmh/20.f), (int)(maxKmh/10.f));
	glColor4f(GL_COLOR_RED);
	glLineWidth(2.0f);
	orthoDraw.drawLine(centerX, centerY, centerX + NxMath::cos(angle)*radius*0.9f, centerY - NxMath::sin(angle)*radius*0.9f);
	glLineWidth(1.0f);
	

	// RPM
	if(curVehicle->getMotor())
	{
		float rpm = curVehicle->getMotor()->getRpm();
		float maxRpm = curVehicle->getMotor()->getMaxRpm();
		float rpm01 = rpm / maxRpm;
		float angle = (1-rpm01) * NxMath::degToRad(210.f) + rpm01*NxMath::degToRad(-30.f);
		glColor4f(GL_COLOR_WHITE);
		orthoDraw.drawCircle(centerX, centerY2, radius, -30, 210, 10, 5, 10);
		glColor4f(GL_COLOR_RED);
		glLineWidth(2.0f);
		orthoDraw.drawLine(centerX, centerY2, centerX + NxMath::cos(angle)*radius*0.9f, centerY2 - NxMath::sin(angle)*radius*0.9f);
		glLineWidth(1.0f);
	}

	NxReal wheelRpmRadius = 30.f;
	float maxWheelRpm = 1000.f;
	for (int i = 0; i < curVehicle->getNbWheels(); i++)
	{
		const zpv::Wheel* curWheel = curVehicle->getWheel(i);
		centerX = spacing + wheelRpmRadius;
		centerY = spacing + wheelRpmRadius + i*(2*wheelRpmRadius + spacing);
		if(curWheel->getWheelFlag(zpv::NX_WF_ACCELERATED))
		{
			glColor4f(GL_COLOR_WHITE);
		} 
		else
		{
			glColor4f(GL_COLOR_LIGHT_GREY);
		}
		orthoDraw.drawCircle(centerX, centerY, wheelRpmRadius, -30, 210, 10, 10, 20);

		float rpm = curWheel->getRpm();
		float rpm01 = rpm / maxWheelRpm;
		float angle = (1-rpm01) * NxMath::degToRad(210.f) + rpm01*NxMath::degToRad(-30.f);
		glColor4f(GL_COLOR_RED);
		glLineWidth(2.0f);
		orthoDraw.drawLine(centerX, centerY, centerX + NxMath::cos(angle)*wheelRpmRadius*0.9f, centerY - NxMath::sin(angle)*wheelRpmRadius*0.9f);
		glLineWidth(1.0f);
	}

}