// NOTE: To run, it is recommended not to be in Compiz or Beryl, they have shown some instability.

#include "main.h"

RenderTimer::RenderTimer(wxGLCanvasSubClass* pane) : wxTimer()
{
    RenderTimer::pane = pane;
}
 
void RenderTimer::Notify()
{
    pane->Refresh();
}
 
void RenderTimer::start()
{
    wxTimer::Start(10);
}

void wxGLCanvasSubClass::OnSize(wxSizeEvent& event)
{
    Refresh();
    //skip the event.
    event.Skip();
}
 
BEGIN_EVENT_TABLE(wxGLCanvasSubClass, wxGLCanvas)
    EVT_PAINT    (wxGLCanvasSubClass::paintEvent)
	EVT_KEY_DOWN (wxGLCanvasSubClass::OnKeyDown)
    EVT_KEY_UP   (wxGLCanvasSubClass::OnKeyUp)
	EVT_SIZE     (wxGLCanvasSubClass::OnSize)
END_EVENT_TABLE()

void wxGLCanvasSubClass::OnKeyDown(wxKeyEvent& event)
{
	zpv::InputManager::getSingletonRef().
		callback_key( event.GetKeyCode(), this->GetSize().x, this->GetSize().y );

    event.Skip();
}

void wxGLCanvasSubClass::OnKeyUp(wxKeyEvent& event)
{
	zpv::InputManager::getSingletonRef().
		callback_keyUp( event.GetKeyCode(), this->GetSize().x, this->GetSize().y );

    event.Skip();
}

void wxGLCanvasSubClass::paintEvent(wxPaintEvent& evt)
{
    wxPaintDC dc(this);
    Render(dc);
}
 
void wxGLCanvasSubClass::paintNow()
{
    wxClientDC dc(this);
    Render(dc);
}

wxGLCanvasSubClass::wxGLCanvasSubClass(wxFrame *parent)
:wxGLCanvas(parent, wxID_ANY,  wxDefaultPosition, wxDefaultSize, 0, wxT("GLCanvas")){
    int argc = 1;
    char* argv[1] = { wxString((wxTheApp->argv)[0]).char_str() };

    glutInit(&argc, argv);
	SetBackgroundStyle( wxBG_STYLE_CUSTOM );
	
	// Setup default render states
	glClearColor(0.3f, 0.4f, 0.5f, 1.0);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	glEnable(GL_CULL_FACE);
	
	// Setup lighting
	glEnable(GL_LIGHTING);
	float AmbientColor[] = { 0.0f, 0.1f, 0.2f, 0.0f };
	glLightfv(GL_LIGHT0, GL_AMBIENT, AmbientColor);
	float DiffuseColor[] = { 1.0f, 1.0f, 1.0f, 0.0f };
	glLightfv(GL_LIGHT0, GL_DIFFUSE, DiffuseColor);
	float SpecularColor[] = { 0.0f, 0.0f, 0.0f, 0.0f };
	glLightfv(GL_LIGHT0, GL_SPECULAR, SpecularColor);
	float Position[] = { 100.0f, 100.0f, 400.0f, 1.0f };
	glLightfv(GL_LIGHT0, GL_POSITION, Position);
	glEnable(GL_LIGHT0);

	NxPhysicsSDK* physicsSDK = zpv::PhysXManager::getSingletonRef().getSDK();
	// Initialize physics SDK and scene
	if ( !physicsSDK )
		return;

	NxReal xpos = 120;
	NxReal ypos = 2.0f;
	NxReal zdist = 5.0f;
	NxReal zpos = 10.f;

	zpv::VehicleFactory& vehicleFactory = zpv::VehicleFactory::getSingletonRef();

	vehicleFactory.createTestCarWithDesc( NxVec3( xpos, ypos, zpos + zdist * -1 ),
	                                                      true,
														  false,
														  false,
														  false,
														  false,
														  physicsSDK );

	vehicleFactory.createCarWithDesc( NxVec3( xpos, ypos, zpos + zdist * 0 ),
		                                                      true,
															  false,
															  false,
															  false,
															  false,
															  physicsSDK );

	/*createCarWithDesc( NxVec3( xpos, ypos, zpos + zdist * 1 ),
	                     true,
						 false,
						 false,
						 false,
						 true,
						 physicsSDK ); old style "1 - normal fw car\n" */

	vehicleFactory.createCarWithDesc( NxVec3( xpos, ypos, zpos + zdist * 1 ),
		                              false,
									  true,
									  true,
									  false,
									  false,
									  physicsSDK );//corvette rw

	vehicleFactory.createCart( NxVec3( xpos, ypos, zpos + zdist * 2 ),
		                       false,
							   true,
							   false );//cart rw

	vehicleFactory.createCarWithDesc( NxVec3( xpos, ypos, zpos + zdist * 3 ),
		                              true,
									  true,
									  false,
									  true,
									  false,
									  physicsSDK ); //old style box style monster truck, 4x4

	vehicleFactory.createTruckPuller( NxVec3( xpos, ypos, zpos + zdist * 4 ),
		                              10,
									  false ); //truck

	vehicleFactory.createFullTruck( NxVec3( xpos, ypos, zpos + zdist * 6 ),
		                            32,
									false,
									false ); //full truck

	vehicleFactory.createFullTruck( NxVec3( xpos, ypos, zpos + zdist * 7 ),
		                            32,
									true,
									false ); //full truck 4 axis

	vehicleFactory.createTruckWithTrailer1( NxVec3( xpos-50, ypos, zpos + zdist * 3 ),
		                                     15,
											 false ); //truck w trailer

	vehicleFactory.createFullTruckWithTrailer2( NxVec3(xpos-50, ypos, zpos + zdist * 5 ),
		                                        16,
												false ); //full truck 4 axis w trailer

	zpv::VehicleManager::selectNext();
	zpv::VehicleManager::selectNext();	//select 1st car.

}

void wxGLCanvasSubClass::Render( wxDC& dc )
{
    SetCurrent();

/** TODO: Here to put the render code */

		// Setup default render states
	glClearColor(0.3f, 0.4f, 0.5f, 1.0);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	glEnable(GL_CULL_FACE);
	
	// Setup lighting
	glEnable(GL_LIGHTING);
	float AmbientColor[] = { 0.0f, 0.1f, 0.2f, 0.0f };
	glLightfv(GL_LIGHT0, GL_AMBIENT, AmbientColor);
	float DiffuseColor[] = { 1.0f, 1.0f, 1.0f, 0.0f };
	glLightfv(GL_LIGHT0, GL_DIFFUSE, DiffuseColor);
	float SpecularColor[] = { 0.0f, 0.0f, 0.0f, 0.0f };
	glLightfv(GL_LIGHT0, GL_SPECULAR, SpecularColor);
	float Position[] = { 100.0f, 100.0f, 400.0f, 1.0f };
	glLightfv(GL_LIGHT0, GL_POSITION, Position);
	glEnable(GL_LIGHT0);

	int xSize = this->GetSize().x;
	int ySize = this->GetSize().y;

	RenderCallback(  xSize, ySize );
	ReshapeCallback( xSize, ySize );

    glFlush();
    SwapBuffers();
}
 
IMPLEMENT_APP(MyApp)
 
class MyFrame : public wxFrame
{
    RenderTimer* timer;
	wxGLCanvasSubClass* drawPane;
    
public:
    MyFrame() : wxFrame((wxFrame *)NULL, -1,  wxT("PhysX raycast cars"), wxPoint(50,50), wxSize(640,480))
    {
        wxBoxSizer* sizer = new wxBoxSizer(wxHORIZONTAL);
        drawPane = new wxGLCanvasSubClass( this );
        sizer->Add(drawPane, 1, wxEXPAND);
        SetSizer(sizer);
        
        timer = new RenderTimer(drawPane);
        Show();
        timer->start();
    }
    ~MyFrame()
    {
		zpv::PhysXManager::getSingletonRef().releaseSDK();
        delete timer;
    }
    void onClose(wxCloseEvent& evt)
    {
        timer->Stop();
        evt.Skip();
    }
    DECLARE_EVENT_TABLE()
};
 
 
BEGIN_EVENT_TABLE(MyFrame, wxFrame)
EVT_CLOSE(MyFrame::onClose)
END_EVENT_TABLE()
 
bool MyApp::OnInit()
{
    frame = new MyFrame();
    frame->Show();
 
    return true;
} 