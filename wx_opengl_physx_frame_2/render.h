#ifndef __RENDER_H__
#define __RENDER_H__

#include "Zglor/Zglor.h"

void RenderCallback(int _width, int _height);
void RenderTerrain();
void ReshapeCallback(int width, int height);
void RenderAllActors();
void renderHUD( zglor::OrthographicDrawing& orthoDraw, int _width, int _height );

#endif // __RENDER_H__